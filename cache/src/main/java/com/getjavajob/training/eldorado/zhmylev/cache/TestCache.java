package com.getjavajob.training.eldorado.zhmylev.cache;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by ZhmylevAA on 16.06.2015.
 */
public class TestCache {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Cache cache = new Cache();
        executor.scheduleAtFixedRate(new CacheCleaner(cache), 5, 5, TimeUnit.SECONDS);

        while (true) {
            String s = reader.readLine();
            if (s.equals("exit")) {
                executor.shutdown();
                System.out.println(cache.getCache());
                break;
            }
            if (cache.put(s)) {
                System.out.println("Your message cached");
            } else {
                System.out.println("Already in the cache:");
                System.out.println(cache.getCachedString(s))
                ;
            }
        }
    }
}






