package com.getjavajob.training.eldorado.zhmylev.cache;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by ZhmylevAA on 16.06.2015.
 */
public class Cache {
    private static final long PERIOD = 60 * 60 * 1000;

    private ConcurrentMap<CachedString, String> cache = new ConcurrentSkipListMap<>();

    public Map<CachedString, String> getCache() {
        return cache;
    }

    public Set<CachedString> getKeySet() {
        return cache.keySet();
    }

    public String getCachedString(String s) {
        return cache.get(new CachedString(s, System.currentTimeMillis() + PERIOD));
    }

    public boolean put(String s) {
        if (s == null) {
            return false;
        }
        return cache.putIfAbsent(new CachedString(s, System.currentTimeMillis() + PERIOD), s) == null;
    }

    public void remove(CachedString cachedString) {
        cache.remove(cachedString);
    }
}