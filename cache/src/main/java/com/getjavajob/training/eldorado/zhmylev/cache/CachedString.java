package com.getjavajob.training.eldorado.zhmylev.cache;

/**
 * Created by ZhmylevAA on 16.06.2015.
 */
public class CachedString implements Comparable<CachedString> {
    private String s;

    private long expireDate;

    public CachedString(String s, long expireDate) {
        this.s = s;
        this.expireDate = expireDate;
    }

    public String getString() {
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CachedString that = (CachedString) o;

        return !(s != null ? !s.equals(that.s) : that.s != null);

    }

    @Override
    public int hashCode() {
        return s != null ? s.hashCode() : 0;
    }

    public long getExpireDate() {
        return expireDate;
    }

    @Override
    public int compareTo(CachedString o) {
        if (o == null) {
            return 1;
        }
        if (s.equals(o.getString())) {
            return 0;
        } else if (expireDate > o.getExpireDate()) {
            return 1;
        } else {
            return -1;
        }
    }
}