package com.getjavajob.training.eldorado.zhmylev.cache;

import java.util.Set;

/**
 * Created by ZhmylevAA on 16.06.2015.
 */
public class CacheCleaner implements Runnable {
    private Cache cache;

    public CacheCleaner(Cache cache) {
        this.cache = cache;
    }

    public void run() {
        System.out.println("Starting cleaning");
        CachedString cleanerString = new CachedString(null, System.currentTimeMillis());
        Set<CachedString> cacheSet = cache.getKeySet();
        for (CachedString cachedString : cacheSet) {
            if (cachedString.compareTo(cleanerString) < 0) {
                cache.remove(cachedString);
            } else {
                break;
            }
        }
    }
}
