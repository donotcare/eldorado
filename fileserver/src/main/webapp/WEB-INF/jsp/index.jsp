<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="login.jsp" %>
<html>
<head>
    <title>PDF getter</title>    
    <style>
   	<%@include file='css/style.css'%>
    </style>
</head>
<body>
<table>
    <tr>
    <th>File names:</th>
    </tr>
    <c:forEach var="uuid" items="${sessionScope.uuids}">
        <tr>
        <td><a href="${uuid.getKey()}">${uuid.getValue()}</a></td>
        </tr>
    </c:forEach>
</table>	
</body>
</html>