package com.getjavajob.training.eldorado.zhmylev.fileserver.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;

/**
 * Created by ZhmylevAA on 17.06.2015.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="I/O exception")
public class HttpIOException extends IOException {
    public HttpIOException(Throwable cause) {
        super(cause);
    }
}
