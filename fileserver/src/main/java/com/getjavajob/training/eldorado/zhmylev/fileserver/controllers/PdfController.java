package com.getjavajob.training.eldorado.zhmylev.fileserver.controllers;

import com.getjavajob.training.eldorado.zhmylev.fileserver.exceptions.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by ZhmylevAA on 16.06.2015.
 */
@Controller
public class PdfController {
    private static final String FILEPATH = "/WEB-INF/downloads/";
    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;

    @RequestMapping("doLogin.html")
    public String login(@RequestParam String login, @RequestParam String password, HttpSession session) {
        if (login.equals(this.login) && password.equals(this.password)) {
            session.setAttribute("user", login);
        }
        return "redirect:/index.html";
    }

    @ExceptionHandler({HttpFileNotFound.class, HttpIOException.class, AuthorizationException.class})
    @ResponseBody
    @RequestMapping(value = "{id}", produces = "application/pdf")
    public FileSystemResource viewPDF(@PathVariable("id") String id,
                                      HttpSession session, HttpServletResponse response) throws HttpFileNotFound, AuthorizationException {
        if (session.getAttribute("user") == null) {
            throw new AuthorizationException();
        }
        Map<String, String> uuids = (Map<String, String>) session.getAttribute("uuids");
        if (uuids == null) {
            throw new HttpFileNotFound();
        }
        String name = uuids.get(id);
        if (name == null) {
            throw new HttpFileNotFound();
        }
        uuids.remove(id);
        ServletContext context = session.getServletContext();
        String appPath = context.getRealPath("");
        String fullPath = appPath + FILEPATH + name;
        System.out.println(fullPath);
        File downloadFile = new File(fullPath);
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);
        return new FileSystemResource(downloadFile);
    }

    @RequestMapping("index.html")
    public ModelAndView index(HttpSession session) {
        if (session.getAttribute("user") != null) {
            ServletContext context = session.getServletContext();
            String appPath = context.getRealPath("");
            Set<String> filePaths = context.getResourcePaths(FILEPATH);
            Map<String, String> uuids = new HashMap<>();
            for (String path : filePaths) {
                uuids.put(UUID.randomUUID().toString(), new File(appPath + path).getName());
            }
            if (!uuids.isEmpty()) {
                session.setAttribute("uuids", uuids);
            }
        }
        return new ModelAndView("index");
    }

    @RequestMapping("logout.html")
    public String logout(HttpSession session) {
        session.removeAttribute("user");
        session.removeAttribute("uuids");
        return "redirect:/index.html";
    }
}
