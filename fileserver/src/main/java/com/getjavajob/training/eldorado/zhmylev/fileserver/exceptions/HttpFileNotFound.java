package com.getjavajob.training.eldorado.zhmylev.fileserver.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.FileNotFoundException;

/**
 * Created by ZhmylevAA on 17.06.2015.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such file")
public class HttpFileNotFound extends FileNotFoundException {
    public HttpFileNotFound() {
    }

}
