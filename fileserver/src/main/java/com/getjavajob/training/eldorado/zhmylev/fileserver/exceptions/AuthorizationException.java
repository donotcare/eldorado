package com.getjavajob.training.eldorado.zhmylev.fileserver.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ZhmylevAA on 18.06.2015.
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason="Unauthorized")
public class AuthorizationException extends Exception {
}
